package com.example.demo.repositorios;
import com.example.demo.entidades.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface ArtistaRepositorio extends CrudRepository<Artista, Long> {  

}