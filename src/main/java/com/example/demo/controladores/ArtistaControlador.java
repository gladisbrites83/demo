package com.example.demo.controladores;
import com.example.demo.entidades.*;
import com.example.demo.servicios.*;
import javax.util.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;




@RestController
@RequestMapping("artistas")
public class ArtistaControlador()
{
    @Autowired
    ArtistaServicio artistaServicio;
    @GetMapping
    private List<Artista> index()
    {
        return artistaServicio.getAll();
    }
}