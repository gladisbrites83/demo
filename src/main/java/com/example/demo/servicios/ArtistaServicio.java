package com.example.demo.servicios;

import java.util.*;
import com.example.demo.repositorios.ArtistaRepositorio;
import com.example.demo.entidades.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class ArtistaServicio {
@Autowired
    ArtistaRepositorio artistaRepositorio;
    public List<Artista> getAll(){
        List<Artista> lista = new ArrayList<Artista>(); 
        artistaRepositorio.findAll().forEach(registro -> lista.add(registro));
        return lista;
    }
    public Artista getById(Long id){ 
        return artistaRepositorio.findById(id).get();

    }
    public void save(Artista artista){
        artistaRepositorio.save(artista);

    }
    public void delete(Long id){
        artistaRepositorio.deleteById(id);
    }
}