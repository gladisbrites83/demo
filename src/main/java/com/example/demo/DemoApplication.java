package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
@SpringBootApplication
@RestController
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
@GetMapping("/")
public String inicio(){
	return "hola chicos, bienvenidos al curso";
}
@GetMapping("/hola")
public String hola(@RequestParam(value="nombre", defaultValue ="mundo") String nombre){
	return  String.format("¡hola %s!", nombre);
}
@GetMapping("/cuadrado")
public String cuadrado(@RequestParam(value="x", defaultValue ="1") int x){
	return  "el cuadrado de "+x+ " es "+ x*x;
}
}
